const express = require('express');
const hbs = require('express-handlebars');
const path = require('path');
const morgan = require('morgan');
const flash = require('connect-flash');
const session = require('express-session');
const MySQLStore = require('express-mysql-session');
const passport = require('passport');

const {database} = require('./keys');
const {IndexRoutes,AuthRoutes, LinksRoutes} = require('./routes/index.routes');

//inicializacion
const app = express();
require('./lib/passport');


//Settings
app.set('views', path.join(__dirname, 'views') )

app.engine('.hbs', hbs({
    defaultLayout:'main',
    layoutsDir: path.join(app.get('views'), 'layouts'),
    partialsDir:path.join(app.get('views'),'partials'),
    extname: '.hbs',
    helpers: require('./lib/handlebars')
}));

app.set('view engine', '.hbs')

//Middleware
app.use(session({ 
    secret:'cualquierpalabraquefuncione',
    resave: false,
    saveUninitialized: false,
    store: new MySQLStore(database)
}))
app.use(flash())
.use(morgan('dev'))
.use(express.urlencoded({extended: false}))
.use(express.static(path.join(__dirname,'public')))
.use(passport.initialize())
.use(passport.session());


app.use((req, res, next)=>{
    app.locals.success =  req.flash('success');
    app.locals.message =  req.flash('message');
    app.locals.user = req.user;
    next();
});

//Routes
app.use(IndexRoutes)
    .use('/links', LinksRoutes)
    .use(AuthRoutes)



//Starting the server
app.listen(process.env.PORT || 4000, () =>  {
    console.log(`Running on port ${process.env.PORT}`)
});

