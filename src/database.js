const mysql = require('mysql2/promise');

const {database} = require('./keys')
 
const pool = mysql.createPool(database);

pool.getConnection((err, conn) => {
    if(err){
        if(err.code == 'PROTOCOL_CONNECTION_LOST'){
            console.error('Database connection was closed');
        }
        if(err.code == 'ER_CON_COUNT_ERROR'){
            console.error('Database connection was closed');
        }
        if(err.code == 'ECONNREFUSED'){
            console.error('Database connection was refused ');
        }
    }  
    if(conn){
        conn.release();
    }
    console.log('DB is connected');
    return;
});

module.exports = pool;
