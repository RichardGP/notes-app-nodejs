const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const pool = require('../database')
const  helpers = require('../lib/helper');


passport.use('local.signin', new LocalStrategy({
    usernameField:'username',
    passwordField: 'password',
    passReqToCallback : true
}, async(req, username, password, done) =>{
    const [rows, fields] = await pool.query('SELECT * FROM users WHERE username = ?', [username]);
    if(rows.length > 0){
        
        const user = rows[0];
        console.log(password, user.password);
        const validPassword = await helpers.comparePassword(password, user.password);
        console.log(validPassword)
        if(validPassword){
            done(null, user, req.flash('success','Welcome'+user.username));
        }else{
            done(null, false, req.flash('message', 'Incorrect password'));
        }
    }else{
        return done(null, false, req.flash('message','username does not exists'));
    }

}));


passport.use('local.signup', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true
}, async (req, username, password, done) => {

    const newUser = {
        username,
        password,
        fullname: req.body.fullname
    }
    newUser.password = await helpers.encryptPassword(password);
    const result = await pool.query('INSERT INTO users SET ?', [newUser]);
    newUser.id = result[0].insertId;
    return done(null, newUser);
}));

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
    const [user, fields] = await pool.query('SELECT * FROM users WHERE id = ?', [id]);
    done(null, user[0]);
});