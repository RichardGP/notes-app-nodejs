CREATE DATABASE database_links;

USE database_links;

CREATE TABLE users(
    id INT NOT NULL AUTO_INCREMENT,
    username varchar(16) not null,
    password varchar(60) not null,
    fullname varchar(100) not null,
    PRIMARY KEY(id)
);

-- LINS TABLE

CREATE TABLE links(
    id INT NOT NULL AUTO_INCREMENT,
    title varchar(150) not null,
    url varchar(255) not null,
    description text,
    user_id int not null,
    created_at timestamp not null default current_timestamp,
    PRIMARY key(id),
    constraint fk_user foreign key (user_id) references users(id)
);

ALTER TABLE `members` CHANGE COLUMN` full_names` `fullname` char (250) NOT NULL;